# SQL Saturday Demo

## Intro

This is all the demo files for SQL Saturday Phoenix Demo. If you would like to start your Docker Containers SQL journey, you can follow along with this document to try out SQL and containers for yourself.

## Before you begin

In order to try out this lab/demo you will need to have Docker installed on your machine. We will not be covering the install of Docker here, but there are some great instructions from Docker themselves here:

* OSX - [Docker CE for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac)
* Windows - [Docker CE for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows)
* Linux - [Docker CE for Linux](https://docs.docker.com/install/linux/docker-ce/centos/)

Follow the instructions above for your machine and then to make sure you are ready to go, open a powershell window or terminal window and run the following command `docker info` you will get output like below:

```
$ docker info
Containers: 37
 Running: 0
 Paused: 0
 Stopped: 37
Images: 135
Server Version: 18.09.2
Kernel Version: 4.9.125-linuxkit
Operating System: Docker for Mac
OSType: linux
Architecture: x86_64
CPUs: 2
Total Memory: 3.855GiB
Live Restore Enabled: false
Product License: Community Engine
```

If you are using a Windows machine, we will use SQL Server Management Studio to validate that the server is up and running (and that it works just like the Windows version) so if you don't have SQL Server Management Studio go and install that now. [Download SQL Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017)

*NOTE: This document assumes you are connected to the Internet. If you do not have a connection to the Internet you will not be able to conduct any of the following commands.*

## Linux SQL Server Containers

When you install Docker CE, it installs in "Linux Native" container mode by default. So we will start with getting a SQL Server Linux container.

Open a terminal window, or a powershell prompt and run the following command:

`docker pull mcr.microsoft.com/mssql/server:2017-latest`

Watch the output. You will see docker go out and pull down each of the layers that makes up the "latest" version of SQL Server 2017. This will take a minute or two depending on your network connection.

### Inspecting a Docker Image

When docker pulls an image, it makes a local cached copy of that image. You can get a list of all the cached images you have by running `docker images`. Here is some example output:

```bash
$ docker images
REPOSITORY                                   TAG                  IMAGE ID            CREATED             SIZE
golang                                       1.12                 b860ab44e93e        2 weeks ago         774MB
registry.gitlab.com/xphyr/azure_okd          latest               21d7c575bfd8        3 weeks ago         529MB
registry.gitlab.com/xphyr/sqlsaturday-demo   latest               cf6b16ff814a        2 months ago        1.4GB
microsoft/mssql-server-linux                 latest               314918ddaedf        4 months ago        1.35GB
mcr.microsoft.com/mssql/server               2017-latest          314918ddaedf        4 months ago        1.35GB
mcr.microsoft.com/mssql/server               2019-CTP2.2-ubuntu   0c6e117b2c2e        4 months ago        2.02GB
```

### Updating a Docker Image

Notice in the output above, the "mssql/server:2017-latest" says its 4 months old? On my demo machine I haven't updated the image in a while. I can update my cached image using the same "docker pull" command but docker will only pull the updated layers:

```
$ docker pull mcr.microsoft.com/mssql/server:2017-latest
2017-latest: Pulling from mssql/server
59ab41dd721a: Already exists 
57da90bec92c: Already exists 
06fe57530625: Already exists 
5a6315cba1ff: Already exists 
739f58768b3f: Already exists 
0b751601bca3: Already exists 
bcf04a22644a: Already exists 
e2df3f3dc0f2: Pull complete 
ac8f190af142: Pull complete 
Digest: sha256:39554141d307f2d40d2abfc54e3a0eea3aa527e58f616496c6f3ed3245a2e2b1
Status: Downloaded newer image for mcr.microsoft.com/mssql/server:2017-latest
```

In the example above, instead of pulling down 1.4Gb of data only 248Mb of updated data was pulled down and I now have a fully up to date image.

### Starting a Container

Let's start our first container. If you don't already have one, open a terminal window, or a powershell prompt and start a SQL Server container:

*Linux/Mac*
```
$ docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=SQL$aturday2019' \
   -p 1433:1433 --name sql1 \
   -d mcr.microsoft.com/mssql/server:2017-latest
```

*Windows/PowerShell*
```
PS> docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=SQL$aturday2019' `
   -p 1433:1433 --name sql1 `
   -d mcr.microsoft.com/mssql/server:2017-latest
```

Now open SQL Server Management Studio and connect to "localhost,1433", using "sa" and the password assigned above when we started the container. Look around, create a new database and a new table and put some data in there. It doesn't have to be much, just enough so you can see if your data persists.

In your terminal run `docker ps -a` this will give you a listing of all containers both running and stopped:

```
$ docker ps -a
CONTAINER ID        IMAGE                                        COMMAND                  CREATED             STATUS              PORTS                    NAMES
44d3120cc188        mcr.microsoft.com/mssql/server:2017-latest   "/opt/mssql/bin/sqls…"   6 seconds ago       Up 4 seconds        0.0.0.0:1433->1433/tcp   sql1
```

Since we gave our running container a name (sql1) we can now refer to that running instance by that name. Let's take a look at the logs for our running container with `docker logs sql1`:

```
$ docker logs sql1
2019-04-29 01:07:08.00 Server      Setup step is copying system data file 'C:\templatedata\master.mdf' to '/var/opt/mssql/data/master.mdf'.
2019-04-29 01:07:08.07 Server      Did not find an existing master data file /var/opt/mssql/data/master.mdf, copying the missing default master and other system database files. If you have moved the database location, but not moved the database files, startup may fail. To repair: shutdown SQL Server, move the master database to configured location, and restart.
2019-04-29 01:07:08.08 Server      Setup step is copying system data file 'C:\templatedata\mastlog.ldf' to '/var/opt/mssql/data/mastlog.ldf'.
2019-04-29 01:07:08.09 Server      Setup step is copying system data file 'C:\templatedata\model.mdf' to '/var/opt/mssql/data/model.mdf'.
2019-04-29 01:07:08.14 Server      Setup step is copying system data file 'C:\templatedata\modellog.ldf' to '/var/opt/mssql/data/modellog.ldf'.
2019-04-29 01:07:08.18 Server      Setup step is copying system data file 'C:\templatedata\msdbdata.mdf' to '/var/opt/mssql/data/msdbdata.mdf'.
2019-04-29 01:07:08.25 Server      Setup step is copying system data file 'C:\templatedata\msdblog.ldf' to '/var/opt/mssql/data/msdblog.ldf'.
2019-04-29 01:07:08.34 Server      Microsoft SQL Server 2017 (RTM-CU14) (KB4484710) - 14.0.3076.1 (X64) 
	Mar 12 2019 19:29:19 
	Copyright (C) 2017 Microsoft Corporation
	Developer Edition (64-bit) on Linux (Ubuntu 16.04.6 LTS)
```

Docker containers are immutable, any data that is stored in them will be deleted when the container is removed. Let's test this out. We are going to stop the docker container we started, and remove the old files. We will then start up a new container and check the status of the SQL database with SSMS.

*Linux/Mac*
```
$ docker stop sql1
$ docker rmi sql1
$ docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=SQL$aturday2019' \
   -p 1433:1433 --name sql1 \
   -d mcr.microsoft.com/mssql/server:2017-latest
```

*Windows/PowerShell*
```
PS> docker stop sql1
PS> docker rmi sql1
PS> docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=SQL$aturday2019' `
   -p 1433:1433 --name sql1 `
   -d mcr.microsoft.com/mssql/server:2017-latest
```

Connect back up to "localhost,1433" with SSMS and see if your database and tables created earlier are there. (Surprise its not.) A database that can't persist data isn't much good so let's fix that problem.

### Persistent Data

There are a few different ways to create a persistent storage area for your container. We will be using [docker volume](https://docs.docker.com/storage/volumes/) mounts which work well across multiple platforms and are the preferred method for persisting data. There is also [docker bind](https://docs.docker.com/storage/bind-mounts/) mounts which allow you to loop a local host directory directly into a container, however these have some limitations and don't always seem to work. Feel free to review the documentation above and try out bind-mounts on your own.

If you haven't already, stop your running SQL container:

```
$ docker stop sql1
$ docker rmi sql1
```

Now start it back up but this time we will add the command in to create a volume and attach it to the "/var/opt/mssql" path within the container. This is the default directory for the SQL data to go in:

*Linux/Mac*
```
$ docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=SQL$aturday2019' \
   -p 1433:1433  \
   -v sqldata:/var/opt/mssql:Z \
   --name sql1 \
   -d mcr.microsoft.com/mssql/server:2017-latest
```

*Windows/PowerShell*
```
PS> docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=SQL$aturday2019' `
   -v sqldata:/var/opt/mssql`
   -p 1433:1433 `
   --name sql1 `
   -d mcr.microsoft.com/mssql/server:2017-latest
```

Connect to the SQL container instance again using SSMS and create a new database, and populate it with some data.  Once you are done, stop the container and remove it.

```
$ docker stop sql1
$ docker rmi sql1
```

Now, start it back up using the same command as before and connect one more time with SSMS, and you will find that your database and its data have survived this time!

*Linux/Mac*
```
$ docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=SQL$aturday2019' \
   -p 1433:1433  \
   -v sqldata:/var/opt/mssql:Z \
   --name sql1 \
   -d mcr.microsoft.com/mssql/server:2017-latest
```

*Windows/PowerShell*
```
PS> docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=SQL$aturday2019' `
   -v sqldata:/var/opt/mssql`
   -p 1433:1433 `
   --name sql1 `
   -d mcr.microsoft.com/mssql/server:2017-latest
```


### Working with Docker Volumes

So now that we can persist data, from container re-start to container re-start, how do you get access to those files directly, OR how do you populate a volume with additional files. Docker gives you a few simple tools to access these volumes. We will cover a few SQL Server specific files below but the process will work for any docker volume you might create.

Start by shutting down the "sql1" instance we have running (you dont want to try and copy a database file that is open and active)

`docker stop sql1`

Lets get access to the new database file (both mdf and ldf) that you created above. In your terminal window or powershell window create a temporary directory to work in and change to that directory. We will also create a test file for later:

```
mkdir ~/sqlsat
cd ~/sqlsat
echo 1 > ~/sqlsat/testfile
```

#### Copying Data Out of a Docker Volume

Now lets copy all the data out of our "sqldata" volume. Since we have not yet deleted the "sql1" container we will reference that to get to the data. Run the following command to copy all the data in the /var/opt/mssql directory to your current directory: (note this will take a minute to run, and there is no progress bar, just wait)

`docker cp sql1:/var/opt/mssql .`

If you now do a "ls" you should find a mssql directory, browse that directory and you will find a structure similar to this:

```
PS> ls


    Directory: C:\Users\mark\sqlsat\mssql


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----        4/29/2019   2:42 PM                .system
d-----        4/29/2019   2:45 PM                data
d-----        4/29/2019   5:48 PM                log
d-----        4/29/2019   2:42 PM                secrets
```

Look inside the "data" directory:

```
PS> ls data
    Directory: C:\Users\mark\sqlsat\mssql\data

Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----        4/29/2019   5:48 PM        4194304 master.mdf
-a----        4/29/2019   5:48 PM        2097152 mastlog.ldf
-a----        4/29/2019   5:48 PM        8388608 model.mdf
-a----        4/29/2019   5:48 PM        8388608 modellog.ldf
-a----        4/29/2019   5:48 PM       14024704 msdbdata.mdf
-a----        4/29/2019   5:48 PM         524288 msdblog.ldf
-a----        4/29/2019   2:46 PM        8388608 tempdb.mdf
-a----        4/29/2019   2:53 PM        8388608 templog.ldf
-a----        4/29/2019   5:48 PM        8388608 testdb1.mdf
-a----        4/29/2019   5:48 PM        8388608 testdb1_log.ldf
```

There are all the database files including your brand new "testdb1" (or whatever you named your database) files. You can copy those files around and share them as needed.

#### Copying Data Into a Docker Volume

Copying files into a Docker Volume is almost as easy. To copy files INTO a docker container just reverse the order of the command:

`docker cp ~/sqlsat/testfile sql1:/var/opt/mssql`

This has placed the "testfile" into the container and placed it at /var/opt/mssql.  If you had some existing database files you wanted to put into the volume you could use a command similar to: 

`docker cp ~/sqlsat/mynewdb* sql1:/var/opt/mssql/data` 

assuming your database files were called "mynewdb.mdf" and "mynewdb_log.ldf".  Once you started the container back up, you could connect to the server using SSMS and attach the new database as you normally would.

## Windows Native SQL Server Containers

If you want, you can also try out a Windows Native SQL Container. In order to do this you will need to be using either a Windows 10, or Server 2016 (or higher) system. You can not run Linux Containers and Windows Containers side by side, you must select either Windows mode or Linux mode. By default Docker will install in Linux mode. To switch open a powershell window and run the following command:

`& $Env:ProgramFiles\Docker\Docker\DockerCli.exe -SwitchWindowsEngine`

Later on if you want to switch back you can run the following command to revert back to the Linux version:

`& $Env:ProgramFiles\Docker\Docker\DockerCli.exe -SwitchLinuxEngine`

Run `docker info` to ensure that you are in Windows mode and lets pull a Windows Native SQL container:

```powershell
PS C:\Users\mark> docker info
Containers: 2
...
Storage Driver: windowsfilter
 Windows:
Logging Driver: json-file
Plugins:
 Volume: local
 Network: ics l2bridge l2tunnel nat null overlay transparent
 Log: awslogs etwlogs fluentd gelf json-file local logentries splunk syslog
Swarm: inactive
Default Isolation: hyperv
Kernel Version: 10.0 17134 (17134.1.amd64fre.rs4_release.180410-1804)
Operating System: Windows 10 Enterprise Version 1803 (OS Build 17134.648)
OSType: windows
Architecture: x86_64
...
Product License: Community Engine
```

Note that the Kernel Version now shows "10.0" and the Operating System is listed as "Windows 10". Now we can pull the Windows Native SQL Container:

`docker pull microsoft/mssql-server-windows-developer`

Sit back and wait, this will take a while. Depending on your Internet connection expect at least 30 minutes for this to complete. The good thing is, in the future this will take MUCH less time as Docker caches all the layers and only pulls the differences between the older and newer images.

Check and see if we pulled down the latest image:

```powershell
PS C:\Users\mark> docker images
REPOSITORY                                 TAG                 IMAGE ID            CREATED             SIZE
microsoft/nanoserver                       latest              4c872414bf9d        6 months ago        1.17GB
microsoft/mssql-server-windows-developer   latest              19873f41b375        15 months ago       15.1GB
```

Just like with the linux container we will specify a sa_password as part of the startup, and we will use a volume mount to persist the data:

```
$ docker run -d -p 1433:1433 -e sa_password='SQL$aturday2019' `
  -e ACCEPT_EULA=Y --name sql1 `
  -v c:\ContainerData:c:\data `
  microsoft/mssql-server-windows-developer
```

## Trying This out with an Application

Let's try and take all of the above pieces and turn it into a running SQL container and a running application. The application we will use is a simple "ToDo" app. The source code for this is located in this same repository.

### Setting up the Database

Copy the empty database files to your local machine from [here](https://gitlab.com/xphyr/sqlsaturday-demo/raw/master/demo_files/EmptyDB.zip?inline=false) and extract them into a working directory. Open up a terminal window or powershell window and change directory to the directory where you extracted the files above. Copy the empty database files into our sqldata Docker volume:

```
docker cp containertest.mdf sql1:/var/opt/mssql/data
docker cp containertest_log.ldf sql1:/var/opt/mssql/data
docker start sql1
```

Connect up to the SQL Server with SQL Server Management Studio. Mount up the "containertest.mdf" file just as you normally would. (If you don't do this all the time follow these steps:)

1. Right Click on "Databases"
2. Select Attach
3. Click "Add"
4. Select "containertest.mdf"
5. Click OK
6. Click OK
 
Assuming the database attaches without issues, we can now move onto starting the application.

### Running the Application

A small demo "ToDo" app has been written [Example-ToDo](https://github.com/xphyr/example-golang-todo) for you to use for testing purposes. There is a binary release for Windows, Linux and OSX that you can find here: [Releases](https://github.com/xphyr/example-golang-todo/releases) Download a copy of the binary for your sepecific OS, unzip it and place it in your working directory.

`./example-golang-todo -user "SA" -password 'SQL$aturday2019' -server localhost -database containertest`

If all goes well, you should see some output stating that the server is listening on port 3000. Open a web browser and connect to http://localhost:3000 Add a few entries, these tasks are now being saved in your SQL Server container.

## CLI Commands

While this document references using SSMS for interacting with your SQL Server container instance, you can also leverage a cli tool inside the Docker image/container. The following commands can be used for testing and mounting new database files.

With your SQL container running, run the following command `docker exec -it sql1 /bin/bash`. You will be given a new command prompt. You are now "inside" the running SQL container. If you run `ps -ef` you can see the sql server running processes.

In addition you can run the sqlcli command to connect to your running server: `/opt/mssql-tools/bin/sqlcmd -U sa -P 'SQL$aturday2019'`

Once connected you can use SQL commands such as the following:

* list databases = "SELECT name, database_id, create_date FROM sys.databases;"
* create a database = "create database golang_todo_dev;"

After each command be sure to enter the command "GO" to process the command.

```sql
root@52aa6a762aa6:/opt/mssql-tools/bin# ./sqlcmd -U sa -P 'SQL$aturday2019'
1> SELECT name, database_id, create_date FROM sys.databases
2> go
1> 
```

## References

[SQL 2017 in Docker](https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-2017)

[SQL 2019 in Docker](https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-ver15)

[SQL 2016 Windows Containers](https://blogs.msdn.microsoft.com/sqlserverstorageengine/2017/02/21/sql-server-2016-developer-edition-in-windows-containers/)

[Contain Storage for Windows Containers](https://docs.microsoft.com/en-us/virtualization/windowscontainers/manage-containers/container-storage)

[SQL Server Management Studio Download](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017#ssms-180-preview-6)

## Credits

I started with this to make our simple webapp, then moved to the SQL Server driver to make it work
[ToDo Web App](https://github.com/westonplatter/example-golang-todo)