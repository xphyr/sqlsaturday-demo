#!/bin/bash
# This script will make the Linux Docker container in feature parity with the 
# windows one for attaching additional databases

# echo '[{"dbName":"mydb","dbFiles":["/data/mydb.mdf","/data/mydb_log.ldf"]},{"dbName":"mydb2","dbFiles":["/data/mydb.mdf","/data/mydb_log.ldf"]}]' | jq -r '.[] | [.dbName, .dbFiles[0], .dbFiles[1]] | @csv' 
# make sure that the server is up /opt/mssql-tools/bin/sqlcmd -S . -U sa -P "$SA_PASSWORD" -Q "SELECT @@VERSION" -
echo "Waiting for SQL Server to start"
/opt/mssql-tools/bin/sqlcmd -S . -U sa -P "$SA_PASSWORD" -Q "SELECT @@VERSION"
while [ $? -ne 0 ]; do
    sleep 10s
    /opt/mssql-tools/bin/sqlcmd -S . -U sa -P "$SA_PASSWORD" -Q "SELECT @@VERSION"
done

#now that we are here, we know SQL server is up.  We need to get the db files we want to mount up
my_dbs=`echo $attach_dbs | jq -r '.[] | [.dbName, .dbFiles[0], .dbFiles[1]] | @csv'`
echo $my_dbs | while IFS="," read dbName dbFile dbLog
do
    echo "Attaching SQL Database"
    echo "dbname: " $dbName
    echo "dbfile: " $dbFile
    echo "dblog:  " $dbLog
    /opt/mssql-tools/bin/sqlcmd -S . -U sa -P "$SA_PASSWORD" \
    -Q "CREATE DATABASE [$dbName] ON (FILENAME = "$dbFile"),(FILENAME = "$dbLog") FOR ATTACH"
done
